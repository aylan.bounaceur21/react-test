# Etape 01 Création Input

Création de l'input qui servira à entrer la chaîne de caractères à rechercher.

```HTML
<Box display="flex" p={2} justifyContent="center">
    <TextField id="standard-basic" label="Enter Search"  onChange={enter} value={search}/>    
    &nbsp;&nbsp;
    <Fab  aria-label="complete"style={{backgroundColor:"#7e57c2",color:"white"}}  onClick={searchitem} >
        <SearchIcon/>
    </Fab>
</Box>
```
## Etape 02 Initialisation d'un tableau

Initialisation d'un tableau afin de pouvoir stocker les `olditems` pour pouvoir les traiter durant la recherche.

```javascript
let storedItems = [];
```

## Etape 03 modification fonction display

La const display a été modifiée afin de stocker les olditems dans `storedItems`.
avant:

```javascript
    const display=()=>{
        displayItem((olditems)=>{
            return [...olditems,additem];
        });
        setItem('');
    }
```

Apres:
```javascript
    const display=()=>{
        displayItem((olditems)=>{
            storedItems = [...olditems,additem];
            return storedItems;
        });
        setItem('');
    }
```

## Etape 04 Création back de l'input

Une const `enter` remplissant la même mission qu'`enterItem` a été faite pour l'input de recherche
afin de prendre en compte l'input qu'au moment de l'actionnement du bouton et non a chaques mofification.

```javascript
    const [search,sets]=useState('');

        const enter=(event)=>{
        sets(event.target.value);
    }
```

## Etape 05 Back de l'input search

Deux tableaux on été créés afin de pouvoir traiter les chaînes de caractères une par une à l'aide d'une boucle `for`.
Utilisation de `.indexOf()` afin de trouver les chaînes de caractères cherchées dans la liste précédemment entrée.


```javascript
    const searchitem=()=>{
        displayItem(()=>{
            let searchedItem = [];
            let searchedResult = [];
            let j = 0;
            for (let i = 0; i < storedItems.length; i++) {
                searchedItem = storedItems[i];
                if(searchedItem.indexOf(search) >= 0) {
                    searchedResult[j] = searchedItem;
                    j++;
                }
            }
            return searchedResult;
        });
        setItem('');
    }
```

## Etape 06 modification des paramètres de l'input

les `onChange` `value` et `onclick` ont été assignés afin de lier l'input et le bouton au back.

```HTML
<TextField id="standard-basic" label="Enter Search"  onChange={enter} value={search}/>    
                    &nbsp;&nbsp;
                    <Fab  aria-label="complete"style={{backgroundColor:"#7e57c2",color:"white"}}  onClick={searchitem} >
                        <SearchIcon/>
                    </Fab>
```

dont : 
```HTML
onChange={enter}
value={search}
onClick={searchitem} 
```

`value={search}` correspond à la chaîne de caractères entrée dans l'input après l'activation du bouton et non à chaque modification grâce à `onChange={enter}`.

`onClick={searchitem}` envoi la chaine de caractères recherchée dans la const "searchitem" afin que tout soit filtré.

## Etape 07 vue finission.

importation du logo "search":

```javascript
`import SearchIcon from '@material-ui/icons/Search'`;
```

utilisation du logo (avec : `<SearchIcon/>`):

```javascript
<Fab  aria-label="complete"style={{backgroundColor:"#7e57c2",color:"white"}}  onClick={searchitem} >
    <SearchIcon/>
</Fab>
```

Ajout de `p={2}` dans la balise box afin de séparer verticalement les deux inputs.

```javascript
<Box display="flex" p={2} justifyContent="center">
    <TextField id="standard-basic" label="Enter Search"  onChange={enter} value={search}/>    
    &nbsp;&nbsp;
    <Fab  aria-label="complete"style={{backgroundColor:"#7e57c2",color:"white"}}  onClick={searchitem} >
        <SearchIcon/>
   </Fab>
</Box>
```
